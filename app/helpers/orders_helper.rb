require 'net/http'

module OrdersHelper
    include BaseHelper
    
    def get_shopify_order_id order_name
        order_id = nil

        uri = URI("https://#{ENV['SHOPIFY_DOMAIN']}/admin/orders.json?access_token=#{ENV['SHOPIFY_API_PASSWORD']}&name=#{order_name}")
        resp = Net::HTTP.get(uri)
        data = JSON.parse resp

        unless data['orders'].empty?
            data['orders'].each do |order|
                if order['name'] == "##{order_name}"
                    order_id = order['id']
                end
            end
        end

        order_id
    end

    def set_tag(order_id, tags)
        result = nil

        unless order_id.nil?
            connect_shopify

            ShopifyGraphQLClient.client.allow_dynamic_queries = true
            result = ShopifyGraphQLClient.query(set_tagsAdd_query("gid://shopify/Order/#{order_id}", tags))
        end

        result
    end

    def set_tagsAdd_query(id, tags)
        query = ShopifyGraphQLClient.parse <<~GRAPHQL
            mutation {
                tagsAdd (id: "#{id}", tags: "#{tags}") 
                {
                    node {
                        id
                    }
                    userErrors {
                        field
                        message
                    }
                }
            }
        GRAPHQL

        query
    end
end
