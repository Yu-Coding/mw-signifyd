module SignifydHelper
    include OrdersHelper

    def verify_signifyd_webhook
        signifyd_key = 'X-SIGNIFYD-SEC-HMAC-SHA256'
        key = "HTTP_#{signifyd_key.gsub('-', '_')}"
        hash = request.headers[key]

        digest = OpenSSL::Digest.new('sha256')
        hmac = OpenSSL::HMAC.digest(digest, ENV['SIGNIFYD_API_KEY'], request.body.read)
        check = Base64.encode64(hmac).strip

        # puts '-------------------'
        # request.headers.each { |key, value| 
        #     puts '//*********'
        #     puts key
        #     puts value
        #     puts '*********//'
        # }
        # puts "hash: #{hash}"
        # puts "check: #{check}"
        # puts '-------------------'

        hash == check
    end

    def signifyd_event
        request.headers[:HTTP_X_SIGNIFYD_TOPIC]
    end

    def signifyd_guaranteeDisposition
        params[:guaranteeDisposition]
    end

    def automate_workflows signifyd_orderId
        # if signifyd_event == 'guarantees/completion'

            puts "orderId in Signifyd webhook: #{signifyd_orderId}"
            native_shopify_order_id = get_shopify_order_id(signifyd_orderId)
            puts "native shopify order id: #{native_shopify_order_id}"

            if native_shopify_order_id.nil?
                # sometimes gets native shopify order id as 'null'
                { status: 300, data: { guarantee: signifyd_guaranteeDisposition } }
            else
                if signifyd_guaranteeDisposition == 'APPROVED'

                    puts "note: guarantee APPROVED"
                    # set 'Signifyd:guaranteeApproved' Tag
                    order = set_tag(native_shopify_order_id, 'Signifyd:guaranteeApproved')
                    puts order.to_json

                    { status: 200, data: { guarantee: 'APPROVED' } }
                elsif signifyd_guaranteeDisposition == 'DECLINED'
                    
                    puts "note: guarantee DECLINED"
                    # set 'Signifyd:guaranteeDeclined' Tag
                    order = set_tag(native_shopify_order_id, 'Signifyd:guaranteeDeclined')
                    puts order.to_json

                    { status: 200, data: { guarantee: 'DECLINED' } }
                end
            end
        # else
        #     puts "note: Signifyd_event is not guarantees/completion"

        #     { event_type: signifyd_event }
        # end
    end
end
