module BaseHelper
    def connect_shopify
        if ShopifyAPI::Base.site.nil?
            ShopifyAPI::Base.site = shop_url
        elsif ShopifyAPI::Base.site != shop_url
            ShopifyAPI::Base.clear_session
            ShopifyAPI::Base.site = shop_url
        end
        ShopifyAPI::Base.api_version = ENV['API_VERSION']
        ShopifyAPI::Base.site
    end

    def shop_url
        URI("https://#{ENV['SHOPIFY_API_KEY']}:#{ENV['SHOPIFY_API_PASSWORD']}@#{ENV['SHOPIFY_DOMAIN']}/admin")
    end
end
