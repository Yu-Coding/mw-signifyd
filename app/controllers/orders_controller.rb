class OrdersController < ApplicationController
    include ApplicationHelper

    skip_before_action :verify_authenticity_token

    def signifyd
        # open('log/signifyd.log', 'a') { |f|
            # @f = f
            puts '---------- START ----------'
            puts "time: #{Time.now.strftime("%I:%M:%S %z")}" 

            # verify Signifyd webhook
            verified = verify_signifyd_webhook

            puts "verified: #{verified}"
            puts "event: #{signifyd_event}"
            puts "guaranteeDisposition: #{signifyd_guaranteeDisposition}"

            if verified
                signifyd_orderId = params[:orderId]
                result = automate_workflows(signifyd_orderId)
                render status: result[:status], json: result[:data]
            else
                render status: 200, json: { error: 'Signifyd webhook is not verified.' }
            end
            puts '---------- END ----------'
        # }
    end
end
